import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from '../main/Main';
import { Provider } from 'react-redux';
import { store } from '../../store/store';

/**
 * Entry point to the application
 */
const App: React.FC = () => {
  return (
    <div className='App'>
      <Provider store={store}>
        <Main />
      </Provider>
    </div>
  );
};

export default App;
