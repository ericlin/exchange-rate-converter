import React from 'react';

/**
 * Error page for non-existing routes
 */
const NotFound: React.FC = () => {
  return (
    <p> Page not found </p>
  );
};

export default NotFound;
