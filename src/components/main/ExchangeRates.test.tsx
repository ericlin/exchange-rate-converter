import React from 'react';
import { shallow } from 'enzyme';
import ExchangeRates from './ExchangeRates';
import {OverTime} from '../../constants/exchangeRatesTypes';

describe('<ExchangeRates />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<ExchangeRates exchangeRateType={OverTime} />);
    expect(wrapper).toMatchSnapshot();
  });
});
