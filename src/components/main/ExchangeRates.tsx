import React from 'react';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import OverTime from './OverTime';
import SpecificDate from './SpecificDate';
import NotFound from '../error/NotFound';
import ExchangeRateButton from '../options/ExchangeRateButton';

import {
  ExchangeRateType,
  OverTime as OverTimeExchange,
  SpecificDate as SpecificDateExchange,
} from '../../constants/exchangeRatesTypes';

/**
 * Determines which exchange rate component to display.
 *
 * @param type
 */
function typeSelector(type: string): JSX.Element {
  switch (type) {
    case OverTimeExchange.type:
      return (<OverTime />);
    case SpecificDateExchange.type:
      return (<SpecificDate />);
    default:
      return (<NotFound />);
  }
}

/**
 * Prop types for ExchangeRates
 */
interface ExchangeRatesProps {
  exchangeRateType: ExchangeRateType;
}

/**
 * Component for housing the buttons for switching between the different
 * exchange rate components and the exchange rate displays.
 *
 * @param props
 */
const ExchangeRates: React.FC<ExchangeRatesProps> = props => {
  const { exchangeRateType } = props;

  return (
    <div>
      <Container>
        <Row>
          <Col xs='12'>
            <h1>Exchange Rate {exchangeRateType.name}</h1>
          </Col>
        </Row>
        <Row>
          <Col xs='3'>
            <ExchangeRateButton exchangeRate={OverTimeExchange} />
            <ExchangeRateButton exchangeRate={SpecificDateExchange} />
          </Col>
          <Col xs='9'>
            {typeSelector(exchangeRateType.type)}
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ExchangeRates;
