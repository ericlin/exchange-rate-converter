import React from 'react';
import Main from './Main';
import { shallow } from 'enzyme';

describe('<Main />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <Main />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
