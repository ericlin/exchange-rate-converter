import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import ExchangeRates from './ExchangeRates';
import {
  OverTime,
  OVERTIME_PATH,
  SpecificDate,
  SPECIFICDATE_PATH,
  Invalid,
  ROOT_PATH,
} from '../../constants/exchangeRatesTypes';

/**
 * Router
 */
const Main: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route path={[OVERTIME_PATH, ROOT_PATH]} exact>
          <ExchangeRates exchangeRateType={OverTime} />
        </Route>
        <Route path={SPECIFICDATE_PATH} exact>
          <ExchangeRates exchangeRateType={SpecificDate} />
        </Route>
        <Route path='*'>
          <ExchangeRates exchangeRateType={Invalid} />
        </Route>
      </Switch>
    </Router>
  );
};

export default Main;
