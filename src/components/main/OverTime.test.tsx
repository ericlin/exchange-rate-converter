import React from 'react';
import { OverTime } from './OverTime';
import { shallow } from 'enzyme';

describe('<SpecificDate />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <OverTime
        order='asc'
        baseCurrency='NZD'
        targetCurrency='AUD'
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
