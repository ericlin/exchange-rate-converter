import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import CurrencyDropdown from '../options/CurrencyDropdown';
import OrderDropdown from '../options/OrderDropdown';
import RatesTable from '../table/RatesTable';
import { AppState } from '../../store/store';
import { BASE, SortValue, TARGET } from '../../constants/exchangeRatesTypes';
import { DateFormat, HistoryEndpoint } from '../../constants/endpoints';
import { descending, Order } from '../../constants/order';
import { HistoryApiData, RowData } from '../../interfaces/dataTypes';
import './css/main.scss';

/**
 * Sort function to order by ascending
 *
 * @param first
 * @param second
 */
function sortDate(first: RowData, second: RowData): SortValue {
  const firstDate = new Date(first.left);
  const secondDate = new Date(second.left);

  if (firstDate > secondDate) {
    return 1;
  }

  if (firstDate < secondDate) {
    return -1;
  }

  return 0;
}

/**
 * Formats the data to be consumed by RatesTable
 *
 * @param data
 * @param target
 */
function formatData(data: HistoryApiData, target: string): RowData[] {
  const rates = data.rates;
  const rowData: RowData[] = [];

  for (const [key, value] of Object.entries(rates)) {
    rowData.push({
      left: key,
      right: {
        key: value[target],
      },
    });
  }

  return rowData;
}

/**
 * Sorts the data in a given direction
 *
 * @param data
 * @param order
 */
function sortData(data: RowData[], order: string): RowData[] {
  const sortedData = data.sort(sortDate);

  // Lazy way of getting it to go the other way...
  if (order === descending.value) {
    return sortedData.reverse();
  }

  return sortedData;
}

/**
 * Fetches the data from the exchange rates API for a time period
 *
 * @param currency
 * @param target
 */
async function getData(currency: string, target: string): Promise<RowData[]> {
  const days = 20;
  const endDate = moment().format(DateFormat);
  const startDate = moment().subtract(days, 'days').format(DateFormat);
  const fetchEndpoint = `${HistoryEndpoint}`
    + `?start_at=${startDate}`
    + `&end_at=${endDate}`
    + `&base=${currency}`;

  try {
    const data = await fetch(fetchEndpoint);
    const json = await data.json() as HistoryApiData;
    return formatData(json, target);
  } catch (e) {
    alert('Error fetching data.');
  }

  return [];
}

/**
 * OverTimeProps types
 */
interface OverTimeProps {
  order: string;
  baseCurrency: string;
  targetCurrency: string;
}

/**
 * This component displays the exchange rate compared with another exchange rate
 * over a period of time, that can be ordered by date ascending and descending.
 *
 * @param props
 */
export const OverTime: React.FC<OverTimeProps> = props => {
  const [data, setData] = useState<RowData[]>([]);
  const { order, baseCurrency, targetCurrency } = props;

  useEffect(() => {
    getData(baseCurrency, targetCurrency)
      .then((result: RowData[]) => {
        const sortedData: RowData[] = sortData(result, order);
        setData(sortedData);
      });
  }, [order, baseCurrency, targetCurrency]);

  return (
    <div>
      <Row className="row justify-content-md-left">
        <Col xs='2'>
          <div>
            <CurrencyDropdown
              currency={baseCurrency}
              identifier={BASE}
              key={BASE}
            />
          </div>
        </Col>
        <Col xs='2'>
          <div className='right-align'>
            <CurrencyDropdown
              currency={targetCurrency}
              identifier={TARGET}
              key={TARGET}
            />
          </div>
        </Col>
      </Row>
      <Row>
        <Col xs='2'>
          <label>
            Order Date:
          </label>
        </Col>
        <Col xs='2'>
          <div className='right-align'>
            <OrderDropdown
              order={order}
            />
          </div>
        </Col>
      </Row>
      <Row>
        <div>
          1 {baseCurrency} vs {targetCurrency}
        </div>
      </Row>
      <Row>
        <RatesTable data={data} leftType='Date' rightType='Rate' />
      </Row>
    </div>
  );
};

/**
 * mapStateToProps types
 */
interface StateFromProps {
  order: Order;
  baseCurrency: string;
  targetCurrency: string;
}

/**
 * Map state to props
 *
 * @param state
 */
const mapStateToProps = (state: AppState): StateFromProps => (
  {
    order: state.orderReducer.order,
    baseCurrency: state.currencyReducer.baseCurrency,
    targetCurrency: state.currencyReducer.targetCurrency,
  }
);

/**
 * Connect component to store
 */
export default connect(mapStateToProps)(OverTime);
