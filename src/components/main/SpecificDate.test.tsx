import React from 'react';
import { SpecificDate, formatData } from './SpecificDate';
import { shallow } from 'enzyme';

describe('<SpecificDate />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <SpecificDate
        date='2019-01-01'
        specificCurrency='NZD'
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('the data formats correctly', () => {
    const dataSet = {
      rates: {
        EUR: 1.1,
        USD: 1.2,
        NZD: 1.3,
      },
      base: 'NZD',
      date: '2019-01-01',
    };

    const expected = [
      {
        left: 'EUR',
        right: {
          key: 1.1,
        },
      },
      {
        left: 'USD',
        right: {
          key: 1.2,
        },
      },
      {
        left: 'NZD',
        right: {
          key: 1.3,
        },
      },
    ];

    const actual = formatData(dataSet);

    expect(actual).toEqual(expected);
  });
});
