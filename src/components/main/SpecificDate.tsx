import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import CurrencyDropdown from '../options/CurrencyDropdown';
import DatePicker from '../options/DatePicker';
import RatesTable from '../table/RatesTable';
import { AppState } from '../../store/store';
import { DateApiData, RowData } from '../../interfaces/dataTypes';
import { DateEndpoint } from '../../constants/endpoints';
import { SPECIFIC } from '../../constants/exchangeRatesTypes';

/**
 * Formats the data to be consumed by RatesTable
 *
 * @param data
 */
export function formatData(data: DateApiData): RowData[] {
  const rates = data.rates;
  const rowData: RowData[] = [];

  for (const [key, value] of Object.entries(rates)) {
    rowData.push({
      left: key,
      right: {
        key: value,
      },
    });
  }

  return rowData;
}

/**
 * Fetches the data from the exchanges rates endpoint
 *
 * @param date
 * @param base
 */
async function getData(date: string, base: string): Promise<RowData[]> {
  try {
    const data = await fetch(`${DateEndpoint}${date}?base=${base}`);
    const json = await data.json() as DateApiData;
    return formatData(json);
  } catch (exception) {
    alert('Error fetching data. Please contact support.');
  }

  return [];
}

/**
 * SpecificDate prop types
 */
interface SpecificDateProps {
  date: string;
  specificCurrency: string;
}

/**
 * Component for looking at exchange rates for a given date.
 *
 * @param props
 */
export const SpecificDate: React.FC<SpecificDateProps> = props => {
  const [data, setData] = useState([] as RowData[]);
  const { date, specificCurrency } = props;

  /**
   * Hook for fetching from the endpoint when the date or exchange rate selected
   * has been changed.
   */
  useEffect(() => {
    if (typeof date !== 'undefined') {
      getData(date, specificCurrency)
        .then((result: RowData[]) => {
            setData(result);
        });
    }
  }, [date, specificCurrency]);

  return (
    <div>
      <div>
        <CurrencyDropdown
          currency={specificCurrency}
          identifier={SPECIFIC}
          key={SPECIFIC}
        />
      </div>
      <div>
        <DatePicker
          date={date}
        />
      </div>
      <div id='rates'>
        <RatesTable data={data} leftType='Currency' rightType='Rate' />
      </div>
    </div>
  );
};

/**
 * Prop types for mapStateToprops
 */
interface StateToProps {
  date: string;
  specificCurrency: string;
}

/**
 * Map state to props
 *
 * @param state
 */
const mapStateToProps = (state: AppState): StateToProps => (
  {
    date: state.dateReducer.date,
    specificCurrency: state.currencyReducer.specificCurrency,
  }
);

/**
 * Connect to store
 */
export default connect(mapStateToProps)(SpecificDate);
