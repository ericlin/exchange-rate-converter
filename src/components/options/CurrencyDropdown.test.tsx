import React from 'react';
import { CurrencyDropdown } from './CurrencyDropdown';
import { shallow } from 'enzyme';

describe('<CurrencyDropdown />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <CurrencyDropdown
        currency='NZD'
        identifier='base'
        setCurrency={jest.fn()}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
