import React from 'react';
import { connect } from 'react-redux';

import { selectableExchangeRates } from '../../constants/exchangeRatesTypes';
import { setCurrency } from '../../store/actions/currencyAction';

/**
 * Populates the list of currency options available
 */
const listOptions = (): JSX.Element[] => {
  return selectableExchangeRates.map(currency => {
    return <option value={currency} key={currency}>{currency}</option>;
  });
};

interface CurrencyDropdownProps {
  currency: string;
  identifier: string;
  setCurrencyValue: (id: string, currency: string) => void;
}

/**
 * Dropdown for selecting individual currencies
 *
 * @param props
 */
export const CurrencyDropdown: React.FC<CurrencyDropdownProps> = props => {
  const { currency, identifier, setCurrencyValue } = props;

  return (
    <div>
      <select
        value={currency}
        onChange={
          (event: React.ChangeEvent<HTMLSelectElement>) => {
            setCurrencyValue(identifier, event.currentTarget.value);
          }
        }
      >
        {listOptions()}
      </select>
    </div>
  );
};

/**
 * Map dispatch to props
 */
const mapDispatchToProps = {
  setCurrencyValue: setCurrency,
};

/**
 * Connect to store
 */
export default connect(
  null,
  mapDispatchToProps,
)(React.memo(CurrencyDropdown));
