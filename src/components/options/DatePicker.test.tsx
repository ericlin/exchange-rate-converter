import React from 'react';
import { DatePicker } from './DatePicker';
import { shallow } from 'enzyme';

describe('<DatePicker />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <DatePicker
        date='2019-01-01'
        setDate={jest.fn()}
        />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
