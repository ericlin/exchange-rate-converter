import React from 'react';
import { setDate } from '../../store/actions/dateAction';
import { connect } from 'react-redux';

/**
 * Date picker prop types
 */
interface DatePickerProps {
  date: string;
  setDateField: (arg: string) => void;
}

/**
 * Date picker component
 *
 * @param props
 */
export const DatePicker: React.FC<DatePickerProps> = props => {
  const { date, setDateField } = props;

  return (
    <input
      type="date"
      id="date-picker"
      value={date}
      onChange={
        (event: React.FormEvent<HTMLInputElement>): void => {
          setDateField(event.currentTarget.value);
        }
      }
    />
  );
};

/**
 * Map dispatch to props
 */
const mapDispatchToProps = {
  setDateField: setDate,
};

/**
 * Connect to store
 */
export default connect(null, mapDispatchToProps)(DatePicker);
