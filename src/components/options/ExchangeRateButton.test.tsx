import React from 'react';
import ExchangeRateButton from './ExchangeRateButton';
import { shallow } from 'enzyme';
import {
  OverTime,
  SpecificDate,
  Invalid,
} from '../../constants/exchangeRatesTypes';
import { Link } from 'react-router-dom';

describe('<ExchangeRateButton />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <ExchangeRateButton
        exchangeRate={OverTime}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('updates properties properly', () => {
    const wrapper = shallow(
      <ExchangeRateButton
        exchangeRate={SpecificDate}
      />,
    );

    // Check initial props of the component are correct
    expect(wrapper.find(Link)).toHaveLength(1);
    expect(wrapper.find(Link).prop('to')).toBe('/specific-date');

    wrapper.setProps({
      exchangeRate: Invalid,
    });
    wrapper.update();

    // Check that the new props of the component have been updated
    expect(wrapper.find(Link)).toHaveLength(1);
    expect(wrapper.find(Link).prop('to')).toBe('/not-found');
  });
});
