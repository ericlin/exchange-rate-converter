import React from 'react';
import { Link } from 'react-router-dom';
import { ExchangeRateType } from '../../constants/exchangeRatesTypes';
import Button from 'react-bootstrap/Button';

/**
 * ExchangeRateButton prop types
 */
interface ExchangeRateButtonProps {
  exchangeRate: ExchangeRateType;
}

/**
 * Button used for selecting an exchange rate type to look at
 *
 * @param props
 */
const ExchangeRateButton: React.FC<ExchangeRateButtonProps> = props => {
  const { exchangeRate } = props;

  return (
    <div>
      <Link to={exchangeRate.path}>
        <Button type='submit' block>{exchangeRate.name}</Button>
      </Link>
    </div>
  );
};

export default ExchangeRateButton;
