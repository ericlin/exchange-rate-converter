import React from 'react';
import { OrderDropdown } from './OrderDropdown';
import { shallow } from 'enzyme';

describe('<OrderDropdown />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <OrderDropdown
        order='asc'
        setListOrder={jest.fn()}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
