import React from 'react';
import { connect } from 'react-redux';
import { ascending, descending, Order } from '../../constants/order';
import { setListOrder } from '../../store/actions/orderAction';

/**
 * OrderDropdown prop types
 */
interface OrderDropdownProps {
  order: string;
  setListOrderField: (arg: Order) => void;
}

/**
 * Dropdown used for ordering the exchange rates in order of data
 *
 * @param props
 */
export const OrderDropdown: React.FC<OrderDropdownProps> = props => {
  const { order, setListOrderField } = props;

  return (
    <div id='order-button'>
      <select
        name='order'
        value={order}
        onChange={
          (event: React.ChangeEvent<HTMLSelectElement>): void => {
            setListOrderField((event.currentTarget.value as Order));
          }
        }
      >
        <option value={descending.value}>{descending.humanReadable}</option>
        <option value={ascending.value}>{ascending.humanReadable}</option>
      </select>
    </div>
  );
};

/**
 * Map dispatch to props
 */
const mapDispatchToProps = {
  setListOrderField: setListOrder,
};

/**
 * Connect to store
 */
export default connect(null, mapDispatchToProps)(OrderDropdown);
