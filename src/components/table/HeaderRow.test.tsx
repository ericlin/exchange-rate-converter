import React from 'react';
import HeaderRow from './HeaderRow';
import { shallow } from 'enzyme';

describe('<HeaderRow />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <table>
        <HeaderRow data={['hello', 'world', 'foo', 'bar']} />
      </table>,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
