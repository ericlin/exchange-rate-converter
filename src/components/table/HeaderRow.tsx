import React from 'react';

/**
 * HeaderRow prop types
 */
interface HeaderRowProps {
  data: string[];
}

/**
 * The header row for the table
 *
 * @param props
 */
const HeaderRow: React.FC<HeaderRowProps> = props => {
  const { data } = props;

  return (
    <tr>
      {data.map(header =>
        <th key={header} className='table-header'>{header}</th>)
      }
    </tr>
  );
};

export default HeaderRow;
