import React from 'react';
import RatesRow from './RatesRow';
import { shallow } from 'enzyme';

describe('<RatesRow />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <table>
        <RatesRow left='left' right='right' />
      </table>,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
