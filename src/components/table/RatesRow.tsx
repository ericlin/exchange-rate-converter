import React from 'react';

interface RatesRowProps {
  left: string;
  right: string;
}

/**
 * A single row entry for displaying exchange rate information
 *
 * @param props
 */
const RatesRow: React.FC<RatesRowProps> = props => {
  const { left, right } = props;

  return(
    <tr className='table-row'>
      <td className='row-left'>{left}</td>
      <td className={'row-right'}>{right}</td>
    </tr>
  );
};

export default RatesRow;
