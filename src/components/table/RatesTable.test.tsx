import React from 'react';
import RatesTable from './RatesTable';
import { shallow } from 'enzyme';

describe('<RatesTable />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(
      <RatesTable
        leftType='left'
        rightType='right'
        data={[]}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
