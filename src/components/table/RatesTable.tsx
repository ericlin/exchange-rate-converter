import React from 'react';
import HeaderRow from './HeaderRow';
import RatesRow from './RatesRow';
import { RowData } from '../../interfaces/dataTypes';
import './css/table.scss';

/**
 * RatesTable prop types
 */
interface RatesTableProps {
  leftType: string;
  rightType: string;
  data: RowData[];
}

/**
 * Renders the exchange rate row information
 *
 * @param rowData
 */
const getRows = (rowData: RowData[]): JSX.Element[] => {
  return rowData.map(
    (row: RowData) =>
      <RatesRow
        left={row.left}
        right={row.right.key.toString()}
        key={row.left}
      />,
  );
};

/**
 * Displays the exchange rate information
 *
 * @param props
 */
const RatesTable: React.FC<RatesTableProps> = props => {
  const { leftType, rightType, data } = props;

  return (
    <div id='rates-table' className='exchange-rate-container'>
      <table className='exchange-rate-table'>
        <thead>
          <HeaderRow data={[leftType, rightType]} />
        </thead>
        <tbody>
          {getRows(data)}
        </tbody>
      </table>
    </div>
  );
};

export default RatesTable;
