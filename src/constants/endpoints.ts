/**
 * Base endpoint for fetching by history. Add in start_at and end_at parameter
 * with date in the format of YYYY-MM-DD to get exchange rates between a range
 * of dates
 */
export const HistoryEndpoint = 'https://api.exchangeratesapi.io/history';

/**
 * Base endpoint for fetching by dates. Append date in YYYY-MM-DD format to
 * get the data for the specific date.
 */
export const DateEndpoint = 'https://api.exchangeratesapi.io/';

/**
 * Date format used by the exchange rates api.
 */
export const DateFormat = 'YYYY-MM-DD';
