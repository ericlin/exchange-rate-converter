export const OVERTIME_NAME = 'Over Time';
export const OVERTIME_TYPE = 'overTime';
export const OVERTIME_PATH = '/recent';

export const SPECIFICDATE_NAME = 'Specific Date';
export const SPECIFICDATE_TYPE = 'specificDate';
export const SPECIFICDATE_PATH = '/specific-date';

export const INVALID_NAME = 'Invalid';
export const INVALID_TYPE = 'invalid';
export const INVALID_PATH = '/not-found';

export type exchangeRateName = typeof OVERTIME_NAME
  | typeof SPECIFICDATE_NAME
  | typeof INVALID_NAME;

export type exchangeRateType = typeof OVERTIME_TYPE
  | typeof SPECIFICDATE_TYPE
  | typeof INVALID_TYPE;

export type exchangeRatePath = typeof OVERTIME_PATH
  | typeof SPECIFICDATE_PATH
  | typeof INVALID_PATH;

export const ROOT_PATH = '/';

/**
 * The exchange rate types
 */
export interface ExchangeRateType {
  name: exchangeRateName;
  type: exchangeRateType;
  path: exchangeRatePath;
}

export const OverTime: ExchangeRateType = {
  name: OVERTIME_NAME,
  type: OVERTIME_TYPE,
  path: OVERTIME_PATH,
};

export const SpecificDate: ExchangeRateType = {
  name: SPECIFICDATE_NAME,
  type: SPECIFICDATE_TYPE,
  path: SPECIFICDATE_PATH,
};

export const Invalid: ExchangeRateType = {
  name: INVALID_NAME,
  type: INVALID_TYPE,
  path: INVALID_PATH,
};

// Sorting function values;
const ZERO = 0;
const ONE = 1;
const NEGATIVE_ONE = -1;

export type SortValue = typeof ZERO | typeof ONE | typeof NEGATIVE_ONE;

export const selectableExchangeRates = [
  'NZD',
  'USD',
  'AUD',
  'EUR',
  'PHP',
  'IDR',
  'JPY',
  'PLN',
  'TRY',
];

export const BASE = 'baseCurrency';

export const TARGET = 'targetCurrency';

export const SPECIFIC = 'specificCurrency';
