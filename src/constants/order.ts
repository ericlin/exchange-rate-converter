export const ASCENDING: string = 'asc';

export const DESCENDING: string = 'desc';

export const ascending = {
  value: ASCENDING,
  humanReadable: 'Ascending',
};

export const descending = {
  value: DESCENDING,
  humanReadable: 'Descending',
};

export type Order = typeof ASCENDING | typeof DESCENDING;
