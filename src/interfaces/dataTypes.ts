/**
 * The data structure returned by the exchangeratesapi history endpoint
 */
export interface HistoryApiData {
  base: string;
  end_at: string;
  rates: {
    [key: string]: {
      [key: string]: number;
    };
  };
  start_at: string;
}

/**
 * The data structure returned by the exchangeratesapi specific date endpoint
 */
export interface DateApiData {
  rates: {
    [key: string]: number;
  };
  base: string;
  date: string;
}

/**
 * The data structure used for consuming the API data
 */
export interface RowData {
  left: string;
  right: {
    [key: string]: number;
  };
}
