export const CURRENCY = 'CURRENCY';

/**
 * Format for SetCurrency return
 */
export interface SetCurrencyAction {
  type: typeof CURRENCY;
  payload: {
    key: string;
    currency: string;
  };
}

/**
 * Set Currency action
 *
 * @param key
 * @param currency
 */
export function setCurrency(
  key: string,
  currency: string,
): SetCurrencyAction {
  return {
    type: CURRENCY,
    payload: {
      key,
      currency,
    },
  };
}

export type SetCurrencyType = SetCurrencyAction;
