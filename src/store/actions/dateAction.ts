export const DATE = 'DATE';

/**
 * Format for setDate return.
 */
interface SetDateAction {
  type: string;
  payload: string;
}

/**
 * Set date action
 *
 * @param date
 */
export function setDate(date: string): SetDateAction {
  return {
    type: DATE,
    payload: date,
  };
}

export type SetDateActionType = SetDateAction;
