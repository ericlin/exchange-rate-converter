import { Order } from '../../constants/order';

/**
 * Action type used for switching between orders
 */
export const LIST_ORDER = 'LIST_ORDER';

/**
 * Interface for setListOrder
 */
export interface SetListOrderAction {
  type: string;
  payload: Order;
}

/**
 * Action used to set the list order
 *
 * @param listOrder
 */
export function setListOrder(listOrder: Order): SetListOrderAction {
  return {
    type: LIST_ORDER,
    payload: listOrder,
  };
}

/**
 * The action types allowed
 */
export type ListOrderActionType = SetListOrderAction;
