import { CURRENCY, SetCurrencyType } from '../actions/currencyAction';

export interface CurrencyState {
  baseCurrency: string;     // Exchange rates over time base currency
  targetCurrency: string;   // Exchange rates over time currency to compare with
  specificCurrency: string; // Exchange rate to compare against all other currencies
}

/**
 * Initial state to use
 */
const initialState: CurrencyState = {
  baseCurrency: 'NZD',
  targetCurrency: 'NZD',
  specificCurrency: 'NZD',
};

/**
 * Currency reducer
 *
 * @param state
 * @param action
 */
export function currencyReducer(
  state = initialState,
  action: SetCurrencyType,
): CurrencyState {
  const { type, payload } = action;

  if (type === CURRENCY) {
    return {
      ...state,
      [payload.key]: payload.currency,
    };
  }

  return state;
}
