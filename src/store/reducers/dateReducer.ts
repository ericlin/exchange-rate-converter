import { SetDateActionType, DATE } from "../actions/dateAction";

interface State {
  date: string;
}

const initialState: State = { date: '2019-01-01' };

/**
 * Date reducer
 *
 * @param state
 * @param action
 */
export function dateReducer(
  state = initialState,
  action: SetDateActionType,
): State {
  if (action.type === DATE) {
    return {
      ...state,
      date: action.payload,
    };
  }

  return state;
}
