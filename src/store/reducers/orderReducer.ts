import { Order } from "../../constants/order";

import {
  ListOrderActionType,
  LIST_ORDER,
} from '../actions/orderAction';

interface State {
  order: Order;
}

const initialState: State = { order: 'asc' };

/**
 * order reducer
 *
 * @param state
 * @param action
 */
export function orderReducer(
  state = initialState,
  action: ListOrderActionType,
): State {
  if (action.type === LIST_ORDER) {
    return {
      ...state,
      order: action.payload,
    };
  }

  return state;
}
