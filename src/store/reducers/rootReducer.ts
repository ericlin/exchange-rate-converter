import { combineReducers } from 'redux';
import { orderReducer } from './orderReducer';
import { dateReducer } from './dateReducer';
import { currencyReducer } from './currencyReducer';

export const rootReducer = combineReducers({
  dateReducer,
  orderReducer,
  currencyReducer,
});
